// rf22_test.pde
// -*- mode: C++ -*-
//
// Test code used during library development, showing how
// to do various things, and how to call various functions

#include <SPI.h>
#include <RF22.h>
#include <SoftwareSPI.h>

// Software SPI class allows us to talk to the radio
// via bit-bang software instead of a hardware SPI driver
SoftwareSPIClass Software_spi;

/* // For TX module
#define SDO_pin 9
#define SDI_pin 8
#define SCLK_pin 7
#define IRQ_pin 0
#define nSel_pin 4

#define RF_OUT_INDICATOR A0
#define Red_LED          13
#define SDA   A4
#define SCL   A5
#define FREQUENCY 436.0
//*/

 // For RX module
#define SDO_pin A0
#define SDI_pin A1
#define SCLK_pin A2
#define IRQ_pin 0
#define nSel_pin 4

#define RF_OUT_INDICATOR A4 // PWM_2
#define Red_LED          A3
#define SDA   6 // PWM_5
#define SCL   7 // PWM_6
#define FREQUENCY 434.0

//*/

// Singleton instance of the radio, using softwaer SPI
RF22 rf22(nSel_pin, IRQ_pin, &Software_spi);


//#define SDO_pin 12
//#define SDI_pin 11
//#define SCLK_pin 13


void setup() 
{
  pinMode(Red_LED, OUTPUT);
  digitalWrite(Red_LED, 1);

  pinMode(SDO_pin, INPUT);   //SDO
  pinMode(SDI_pin, OUTPUT);   //SDI
  pinMode(SCLK_pin, OUTPUT);   //SCLK

  Serial.begin(9600);
  Software_spi.setPins(SDO_pin, SDI_pin, SCLK_pin);
  if (!rf22.init())
    Serial.println("RF22 init failed");
  else
    Serial.println("RF22 init success");
    
  Serial.print("Maximum buf: ");
  Serial.println(RF22_MAX_MESSAGE_LEN);
  
  if (!rf22.setFrequency(FREQUENCY))
    Serial.println("setFrequency failed");
  if (!rf22.setModemConfig(RF22::GFSK_Rb2Fd5))
    Serial.println("setModemConfig failed");

}



void loop()
{
  uint8_t buf[RF22_MAX_MESSAGE_LEN];
  uint8_t len = sizeof(buf);
  rf22.waitAvailable();
  if (rf22.recv(buf, &len))
  {
    Serial.println((char*)buf);
    Serial.print("$RPRXM,");
    Serial.print(rf22.lastRssi());
    Serial.println(",RSSI,,*42");
  }
}


