

// Arduino Pro 5V 16MHz Atmega328

#include <SPI.h>
#include <RF22.h>
#include <SoftwareSPI.h>

// Software SPI class allows us to talk to the radio
// via bit-bang software instead of a hardware SPI driver
SoftwareSPIClass Software_spi;

// For TX module
// http://hobbyking.com/hobbyking/store/__40031__OrangeRX_Open_LRS_433MHz_Transmitter_1W_compatible_with_Futaba_radio_.html
#define SDO_pin 9
#define SDI_pin 8
#define SCLK_pin 7
#define IRQ_pin 0
#define nSel_pin 4

#define RF_OUT_INDICATOR A0
#define Red_LED          13
#define SDA   A4
#define SCL   A5

#define FREQUENCY 434.0
//*/


// Singleton instance of the radio, using softwaer SPI
RF22 rf22(nSel_pin, IRQ_pin, &Software_spi);



void setup() 
{
  delay(5000);
  pinMode(Red_LED, OUTPUT);
  digitalWrite(Red_LED, 1);
  pinMode(SDA, OUTPUT);
  pinMode(SCL, OUTPUT);
  pinMode(RF_OUT_INDICATOR, INPUT);
  digitalWrite(SDA, 1);
  digitalWrite(SCL, 1);

  Serial.begin(9600);
  Software_spi.setPins(SDO_pin, SDI_pin, SCLK_pin);
  if (!rf22.init())
    Serial.println("$RPDBG,RF22 init failed");
  else
    Serial.println("$RPDBG,RF22 init success");

  rf22.setTxPower(RF22_TXPOW_17DBM);
  if (!rf22.setFrequency(FREQUENCY))
    Serial.println("$RPDBG,setFrequency failed");
  if (!rf22.setModemConfig(RF22::GFSK_Rb2Fd5))
    Serial.println("$RPDBG,setModemConfig failed");
}



#define MAX_GPS_BUF 150
uint8_t last_gga[MAX_GPS_BUF+1];
uint8_t last_gga_idx = 0;

uint8_t current_gga[MAX_GPS_BUF+1];
uint8_t current_gga_idx = 0;

uint32_t lastTX = micros();
uint32_t lastCam = micros();

void loop()
{
  while(Serial.available())
  {    
    char c = Serial.read();
    Serial.write(c);
    current_gga[current_gga_idx] = (uint8_t)c;
    current_gga_idx = (current_gga_idx + 1)%MAX_GPS_BUF;      
    if(c == '\n')
    {
      if((current_gga[0] == '$') 
          && (current_gga[1] == 'G')
          && (current_gga[2] == 'P')
          && (current_gga[3] == 'G')
          && (current_gga[4] == 'G')
          && (current_gga[5] == 'A'))
      {
        for(int i=0; i<current_gga_idx; i++)
        {
          last_gga[i] = current_gga[i];
        }
        last_gga_idx = current_gga_idx;
        last_gga[last_gga_idx-1] = 0;
      }
      current_gga_idx = 0;
    }
  }
  uint32_t time = micros();
  if((time - lastTX) > 3000000)
  {
    lastTX = time;
    if(last_gga_idx > 0)
    {
      rf22.send(last_gga, last_gga_idx);
      rf22.waitPacketSent();
      char telemetry_buf[100] = "$RPTLM,";
      uint8_t temperature = rf22.temperatureRead();
      itoa(temperature,(char *)&telemetry_buf[strlen(telemetry_buf)], 10);
      strcat(telemetry_buf, ",TEMP,");
      int voltage = analogRead(RF_OUT_INDICATOR);
      itoa(voltage,(char *)&telemetry_buf[strlen(telemetry_buf)], 10);
      strcat(telemetry_buf, ",BATT,,*42");
      rf22.send((uint8_t *)telemetry_buf, strlen(telemetry_buf)+1);
      rf22.waitPacketSent();
    }
  }
  if((time - lastCam) > 1300000)
  {
    lastCam = time;
    digitalWrite(SDA, 1);
    delay(70);
    digitalWrite(SDA, 0);
    delay(30);
  }
}


